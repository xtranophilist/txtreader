
package com.sonyericsson.extras.liveview.plugins.txtreader;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

/**
 * Subclassing DialogPreference for About menu in preferences menu.
 * @author
 * xtranophilist
 */
public class AboutDialogPreference extends DialogPreference {

    public AboutDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

} 