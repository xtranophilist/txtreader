package com.sonyericsson.extras.liveview.plugins.txtreader;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.sonyericsson.extras.liveview.plugins.AbstractPluginService;
import com.sonyericsson.extras.liveview.plugins.PluginConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public class TxtReaderService extends AbstractPluginService {

    // Is loop running?
    private boolean mWorkerRunning = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    @Override
    public void onStart(Intent intent, int startId) {
        Log.e(PluginConstants.LOG_TAG, "Started!");
        super.onStart(intent, startId);
    }

    @Override
    public void onCreate() {
        Log.e(PluginConstants.LOG_TAG, "Created!");
        super.onCreate();
        refreshed = false;
    }

    @Override
    public void onDestroy() {
        Log.e(PluginConstants.LOG_TAG, "Destroyed!");
        super.onDestroy();
    }

    @Override
    protected void button(String buttonType, boolean doublepress, boolean longpress) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void displayCaps(int displayWidthPx, int displayHeigthPx) {
    }

    @Override
    protected void screenMode(int mode) {
        Log.d(PluginConstants.LOG_TAG, "screenMode: screen is now " + ((mode == 0) ? "OFF" : "ON"));
    }

    protected boolean isSandboxPlugin() {
        return false;
    }

    protected void stopWork() {
        Log.e(PluginConstants.LOG_TAG, "Work Stopped!");
        mWorkerRunning = false;
    }

    protected void onServiceConnectedExtended(ComponentName className, IBinder service) {
        Log.d(PluginConstants.LOG_TAG, "Service Connected!");
        AbstractPluginService.unsetRefreshed();
    }

    protected void onServiceDisconnectedExtended(ComponentName className) {
        Log.d(PluginConstants.LOG_TAG, "Service Disconnected!");
    }

    protected void startPlugin() {
        Log.d(PluginConstants.LOG_TAG, "Plugin is Started!");
//        Log.d(PluginConstants.LOG_TAG, "Refreshed : " + AbstractPluginService.isRefreshed());
//        if (AbstractPluginService.isRefreshed()) {
//            Log.d(PluginConstants.LOG_TAG, "Refreshed!");
//            doRefresh();
//        }
        settings = getSharedPreferences("lvtxtreader", 0);
        editor = settings.edit();
        startWork();
    }

    protected void stopPlugin() {
        Log.d(PluginConstants.LOG_TAG, "Plugin Stopped!");
        stopWork();
    }

    protected void onUnregistered() {
        Log.d(PluginConstants.LOG_TAG, "onUnregistered");
        stopWork();
    }

    //Watches changes on preference changes
    protected void onSharedPreferenceChangedExtended(SharedPreferences pref, String key) {
        Log.d(PluginConstants.LOG_TAG, "Preferences changed!");
        if (key.equals("refresh")) {
            Log.d(PluginConstants.LOG_TAG, "Refreshing!");
            doRefresh();
        }
    }

    private void sendAnnounce(String header, String body, String openInPhone) {
        try {
            if (mWorkerRunning && (mLiveViewAdapter != null) && mSharedPreferences.getBoolean(PluginConstants.PREFERENCES_PLUGIN_ENABLED, false)) {
                mLiveViewAdapter.sendAnnounce(mPluginId, mMenuIcon, header, body, System.currentTimeMillis(), openInPhone);
                Log.d(PluginConstants.LOG_TAG, "Announce sent to LiveView");
            } else {
                Log.d(PluginConstants.LOG_TAG, "LiveView not reachable");
            }
        } catch (Exception e) {
            Log.e(PluginConstants.LOG_TAG, "Failed to send announce", e);
        }
    }

    private void sendAnnounce(String header, String body) {
        sendAnnounce(header, body, "");
    }

    private static String readFile(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    protected void startWork() {
        Log.e(PluginConstants.LOG_TAG, "Work Started!");
        // Check if plugin is enabled and it is not already running
        if (!mWorkerRunning && mSharedPreferences.getBoolean(PluginConstants.PREFERENCES_PLUGIN_ENABLED, false)) {
            mWorkerRunning = true;
        }
        read();
    }

    private String getMimeType(File file) {
        Uri fileUri = Uri.fromFile(file);
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString());
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
        return mimeType;
    }

    private void read() {
        File dir = new File(mSharedPreferences.getString("folder", ""));
        Log.d(PluginConstants.LOG_TAG, "Folder to watch: " + dir.getAbsolutePath());
        boolean success = true;
        if (!dir.exists()) {
            //if  /sdcard/Android/documents/ is the preferred non-existing location, create it.
            if (dir.getAbsolutePath().equals("/sdcard/Android/documents/") || dir.getAbsolutePath().equals("/sdcard/Android/documents")) {
                File androidDir = new File("/sdcard/Android/");
                if (!androidDir.exists()) {
                    //create /sdcard/Android/ if it doesn't exist
                    success = (new File("/sdcard/Android/")).mkdir();
                    Log.d(PluginConstants.LOG_TAG, "Creating  /sdcard/Android/" + success);
                }
                if (success) {
                    success = (new File("/sdcard/Android/documents/")).mkdir();
                    Log.d(PluginConstants.LOG_TAG, "Creating  /sdcard/Android/documents" + success);
                }
            } else {
                Log.d(PluginConstants.LOG_TAG, "Error opening " + dir.getAbsolutePath());
                //if the default path is changed and the modified path can't be opened, send error message
                String body = "Error opening directory " + mSharedPreferences.getString("folder", "") + ". Maybe the directory doesn't exist. You can change the location from LiveView Application -> Customize Tiles -> Manage Plugins -> Text Reader plugin -> Folder to Watch. If the issue isn't resolved, please report to http://github.com/xtranophilist/textreader-for-liveview/issues or via e-mail to xtranophilist@gmail.com. Thank you!";
                sendAnnounce("Error!", body, "http://github.com/xtranophilist/textreader-for-liveview/issues/");
                success = false;
            }
        }
        if (success) {
            File[] filelist = dir.listFiles();
            String content;
            for (File file : filelist) {
                try {
                    Log.d(PluginConstants.LOG_TAG, "Found " + file.getAbsolutePath());
                    Log.d(PluginConstants.LOG_TAG, "Length: " + file.length());
                    Log.d(PluginConstants.LOG_TAG, "File Type: " + getMimeType(file));
                    content = readFile(file);
                    //calculate hash to see if the file has been modified
                    MessageDigest m = MessageDigest.getInstance("MD5");
                    m.reset();
                    m.update(content.getBytes());
                    byte[] digest = m.digest();
                    BigInteger bigInt = new BigInteger(1, digest);
                    String hashtext = bigInt.toString(16);
                    Log.d(PluginConstants.LOG_TAG, "Hash from file: " + hashtext);
                    String hash = settings.getString(file.getAbsolutePath(), "");
                    Log.d(PluginConstants.LOG_TAG, "Hash from settings: " + hash);
                    if (file.length() > 0 && getMimeType(file).equalsIgnoreCase("text/plain") && !(hash.equals(hashtext))) {
                        //save filename and its hash
                        editor.putString(file.getAbsolutePath(), hashtext);
                        editor.commit();
                        content = readFile(file);
                        //get rid of stupid non-ASCII characters
                        content = content.replaceAll("[^\\x00-\\x7F]", "");
                        Log.e(PluginConstants.LOG_TAG, "Sending for " + file.getAbsolutePath());
                        if (content.length() < 4096) {//for a single piece text file
                            sendAnnounce(file.getName(), content);
                        } else {
                            //find the number of pieces text file should be broken into
                            int pieces = content.length() / 4080;
                            //send the last peice first, so that it is seen last on device
                            sendAnnounce("|" + (pieces + 1) + "|" + "-" + file.getName(), "... " + content.substring(pieces * 4080));
                            Log.e(PluginConstants.LOG_TAG, "Sent part " + (pieces + 1));
                            int j;
                            String header, body;
                            for (j = pieces; j >= 1; j--) {
                                header = "";
                                body = "";
                                if (j != 1) {
                                    header += "|" + j + "| - ";// |<Part#>|
                                    body += "... ";
                                }
                                header += file.getName();// |<Part#>| - <FileName>
                                body += content.substring((j - 1) * 4080, j * 4080) + " ...";
                                sendAnnounce(header, body);
                                Log.e(PluginConstants.LOG_TAG, "Sent part " + j);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(PluginConstants.LOG_TAG, "Failed to read file " + file.getAbsolutePath());
                }
            }
        }
    }

    private void doRefresh() {
        Log.d(PluginConstants.LOG_TAG, "Clearing file data... ");
        //remove all data
        settings = getSharedPreferences("lvtxtreader", 0);
        editor = settings.edit();
        editor.clear();
        editor.commit();
        read();
    }

    @Override
    protected void openInPhone(String openInPhoneAction) {
    }
}